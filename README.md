# Uploaded File Filename Randomizer module

The Open Worldwide Application Security Project, aka [OWASP](https://en.wikipedia.org/wiki/OWASP)
recommends in its [File Upload Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html)
that files uploaded to web applications are renamed by the application.

This module renames all uploaded files to a random string comprised of characters a-z and 0-9.
The original extension will be kept.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

Drupal.

## Recommended modules

None.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

No configuration is necessary. Any files uploaded after the module is installed will be renamed.

## Maintainers

- [Elliot Ward] - [Eli-T](https://www.drupal.org/u/eli-t)
