<?php

namespace Drupal\uploaded_file_filename_randomizer\EventSubscriber;

use Drupal\Component\Utility\Random;
use Drupal\Core\File\Event\FileUploadSanitizeNameEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Rename all uploaded files.
 */
final class UploadedFileFilenameRandomizerSubscriber implements EventSubscriberInterface {

  /**
   * Randomizes the filename referenced by the Sanitize File Name Event.
   *
   * @param \Drupal\Core\File\Event\FileUploadSanitizeNameEvent $event
   *   File upload sanitize name event.
   */
  public function randomizeName(FileUploadSanitizeNameEvent $event) {
    // Make sure the extension is preserved when we randomize the filename.
    $filename = $event->getFilename();
    $filename_parts = explode('.', $filename);
    $extension = (string) array_pop($filename_parts);

    $event->setFilename((new Random())->machineName(32) . ".{$extension}");
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      FileUploadSanitizeNameEvent::class => ['randomizeName'],
    ];
  }

}
